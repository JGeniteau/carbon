package com.example.carbon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.example.carbon.game.GameHelper;
import com.example.carbon.game.Main;
import com.example.carbon.game.Orientation;
import com.example.carbon.game.TypeCase;
import com.example.carbon.game.entites.Aventurier;
import com.example.carbon.game.entites.Carte;
import com.example.carbon.game.entites.Game;
import com.example.carbon.game.services.FileServices;
import com.example.carbon.game.services.FileServicesImpl;
import com.example.carbon.game.services.GameServices;
import com.example.carbon.game.services.GameServicesImpl;

public class GameTest {
	
	private static final String FILE_EXEMPLE_ENONCE_PATH = "src/test/ressources/fileEnonce.txt";
	private static final String FILE_WITH_COMMENTS_PATH = "src/test/ressources/fileWithComments.txt";
	private static final String FILE_WITH_ERROR_PATH = "src/test/ressources/fileError.txt";
	private static final String FILE_OUTSIDE_PATH = "src/test/ressources/fileOutSide.txt";
	private static final String FILE_RENCONTRE_PATH = "src/test/ressources/fileRencontre.txt";
	
	private FileServices fileServices = new FileServicesImpl();
	private GameServices gameServices = new GameServicesImpl();
	
	@Test
	void importExempleEnonceTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_EXEMPLE_ENONCE_PATH);
		Game game = gameServices.createGameContext(fileData);
		Carte carte = game.getCarte();
		
		assertEquals(12,carte.getNodes().size());
		assertEquals(2,carte.getNodes().stream().filter(s-> TypeCase.MONTAGNE.equals(s.getTypeCase())).collect(Collectors.toList()).size()    );
		assertEquals(1,game.getAventuriers().size());
		assertEquals(1,game.getAventuriers().size());
	}
	
	@Test
	void importFileWithCommentsTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_WITH_COMMENTS_PATH);
		Game game = gameServices.createGameContext(fileData);
		Carte carte = game.getCarte();
		
		assertEquals(12,carte.getNodes().size());
		assertEquals(2,carte.getNodes().stream().filter(s-> TypeCase.MONTAGNE.equals(s.getTypeCase())).collect(Collectors.toList()).size()    );
		assertEquals(1,game.getAventuriers().size());assertEquals(1,game.getAventuriers().size());
	}
	
	@Test
	void importFileErrorTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_WITH_ERROR_PATH);	
		Exception exception = assertThrows(Exception.class, () -> {
			Main.runGame(fileData, gameServices, fileServices);
	    });
		String expectedMessage = "Type d'objet inconnu.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.equals(expectedMessage));
	}
	
	@Test
	void runExempleEnonceTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_EXEMPLE_ENONCE_PATH);
		Game game = Main.runGame(fileData, gameServices, fileServices);
		Aventurier aventurier = game.getAventuriers().get(0);
		
		assertEquals(GameHelper.getNodeByCoord(0, 3, game.getCarte()), aventurier.getNode());
		assertEquals(Orientation.SUD, aventurier.getOrientation());
		assertEquals(3, aventurier.getNbTresors());

	}
	
	@Test
	void runOutSideTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_OUTSIDE_PATH);
		Game game = Main.runGame(fileData, gameServices, fileServices);
		Aventurier aventurier = game.getAventuriers().get(0);
		
		assertEquals(GameHelper.getNodeByCoord(1, 3, game.getCarte()), aventurier.getNode());
		assertEquals(Orientation.SUD, aventurier.getOrientation());

	}
	
	@Test
	void runRencontreTest() throws Exception {
		List<String> fileData = fileServices.readFile(FILE_RENCONTRE_PATH);
		Game game = Main.runGame(fileData, gameServices, fileServices);
		List<Aventurier> aventuriers = game.getAventuriers();
		Aventurier a1 = aventuriers.stream().filter(a ->"Lara".equals(a.getName())).findAny().get();
		Aventurier a2 = aventuriers.stream().filter(a ->"Larra".equals(a.getName())).findAny().get();
		assertEquals(GameHelper.getNodeByCoord(1, 2, game.getCarte()), a1.getNode());
		assertEquals(GameHelper.getNodeByCoord(1, 3, game.getCarte()), a2.getNode());

	}

}
