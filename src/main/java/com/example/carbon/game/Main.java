package com.example.carbon.game;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.carbon.game.entites.Game;
import com.example.carbon.game.services.FileServices;
import com.example.carbon.game.services.FileServicesImpl;
import com.example.carbon.game.services.GameServices;
import com.example.carbon.game.services.GameServicesImpl;

public class Main {
	
	public static Game runGame(List<String> fileData, GameServices gameServices, FileServices fileServices) throws Exception{
		Game game = null;
		game = gameServices.createGameContext(fileData);
		
		for(int i = 0; i < GameHelper.getNbGameTours(game); i++) {
			gameServices.runGameTour(game);
		}
		String fileOutPath = fileServices.getPathFileOut(game);
		fileServices.readFile(fileOutPath);
		return game;
	}
	
	public static List<String> runGame2(List<String> fileData, GameServices gameServices, FileServices fileServices) throws Exception{
		Game game = null;
		game = gameServices.createGameContext(fileData);
		
		for(int i = 0; i < GameHelper.getNbGameTours(game); i++) {
			gameServices.runGameTour(game);
		}
		String fileOutPath = fileServices.getPathFileOut(game);
		
		return fileServices.readFile(fileOutPath);
	}
}
