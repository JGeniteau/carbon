package com.example.carbon.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.carbon.game.entites.Aventurier;
import com.example.carbon.game.entites.Carte;
import com.example.carbon.game.entites.Game;
import com.example.carbon.game.entites.Node;

public class GameHelper {
	
	private GameHelper(){
		
	}
	
	public static void addMontagne(Carte carte, int x, int y, Logger logger) throws Exception {
		Node node = getNodeByCoord(x, y, carte);   
		if(node != null && TypeCase.PLAINE.equals(node.getTypeCase())) {
			node.setTypeCase(TypeCase.MONTAGNE);
			node.setFree(false);
			logger.info(String.format("Montagne ajoutée: %s", node));
		}
		else {
			throw new Exception("Case déjà définie.");
		}
	}
	
	public static void addTresor(Carte carte, int x, int y, int nbTresors, Logger logger) throws Exception {
		Node node = getNodeByCoord(x, y, carte); 
		if(node != null && TypeCase.PLAINE.equals(node.getTypeCase())) {
			node.setTypeCase(TypeCase.TRESOR);
			node.setNbTresors(nbTresors);
			logger.info(String.format("%d Trésors ajouté: %s", nbTresors,node));
		}
		else {
			throw new Exception("Case déjà définie.");
		}
	}
	
	public static void addAventurier(String name, int x, int y, String orientation, String mouvements, Game game, Logger logger) {
		Node node = getNodeByCoord(x, y, game.getCarte());
		if(node != null) {
			Aventurier aventurier = new Aventurier(name, node, GameHelper.convertOrientation(orientation), GameHelper.convertMouvements(mouvements));
			game.getAventuriers().add(aventurier);
			node.setFree(false);
			logger.info(String.format("Aventurier ajoutée: %s", node));
		}
	}
	
	public static void defineCarte(Game game, Carte carte) throws Exception {
		if (game.getCarte() == null) {
			game.setCarte(carte);
		}
		else {
			throw new Exception("Carte déjà existante.");
		}
	}
	
	public static Node getNodeByCoord(int x, int y, Carte carte) {
		return carte.getNodes().stream()
                .filter((p) -> p.getX() == x && p.getY() == y)
                .findAny()
                .orElse(null);
	}
	
	public static void checkTresor(Game game, Aventurier aventurier) {
		Node node = game.getCarte().getNodes().stream().filter(n-> n.equals(aventurier.getNode())).findAny().orElse(null);
		if (node.getTypeCase().equals(TypeCase.TRESOR) && node.getNbTresors() > 0 
				&& !aventurier.getOldNode().equals(aventurier.getNode())) {
			aventurier.addTresor();
			node.setNbTresors(node.getNbTresors()-1);
		}
	}
	
	public static Orientation convertOrientation(String letter) {
		switch (letter) {
		case "N":
			return Orientation.NORD;
		case "S":
			return Orientation.SUD;
		case "E":
			return Orientation.EST;
		case "O":
			return Orientation.OUEST;
		}
		return null;
	}
	
	public static List<Mouvement> convertMouvements(String data) {
		List<Mouvement> mouvements = new ArrayList<>();
		for (String str: data.split("")) {
			switch (str) {
			case "A":
				mouvements.add(Mouvement.AVANCER);
				break;
			case "G":
				mouvements.add(Mouvement.GAUCHE);
				break;
			case "D":
				mouvements.add(Mouvement.DROITE);
				break;
			}
		}
		return mouvements;
	}
	
	public static int getNbGameTours(Game game) {
		List<Aventurier> aventuriers = game.getAventuriers();
		List<Integer> sizeMouvements = new ArrayList<>();
		aventuriers.forEach(a -> sizeMouvements.add(a.getMouvements().size()));
		return Collections.max(sizeMouvements);
	}
	
	public static void deplacementAventurier(Aventurier aventurier, int numTour, Carte carte, Logger logger) {
		Mouvement mouvement = aventurier.getMouvements().get(numTour);
		Orientation orientation = aventurier.getOrientation();
		aventurier.setOldNode(aventurier.getNode());
		switch(mouvement) {
		 case AVANCER:
			 int newX = 0;
			 int newY = 0;
			 switch(orientation) {
			 case NORD:
				 newX = aventurier.getNode().getX();
				 newY = aventurier.getNode().getY() - 1;
				 break;
			 case SUD:
				 newX = aventurier.getNode().getX();
				 newY = aventurier.getNode().getY() + 1;
				 break;
			 case EST:
				 newX = aventurier.getNode().getX() + 1;
				 newY = aventurier.getNode().getY();
				 break;
			 case OUEST:
				 newX = aventurier.getNode().getX() - 1;
				 newY = aventurier.getNode().getY();
				 break;
			 }
		moving(aventurier, getNodeByCoord(newX, newY, carte), logger);
		break;
		 case GAUCHE:
			 switch(orientation) {
			 case NORD:
				 aventurier.setOrientation(Orientation.OUEST);
				 break;
			 case SUD:
				 aventurier.setOrientation(Orientation.EST);
				 break;
			 case EST:
				 aventurier.setOrientation(Orientation.NORD);
				 break;
			 case OUEST:
				 aventurier.setOrientation(Orientation.SUD);
				 break;
			 }
			 break;
		 case DROITE:
			 switch(orientation) {
			 case NORD:
				 aventurier.setOrientation(Orientation.EST);
				 break;
			 case SUD:
				 aventurier.setOrientation(Orientation.OUEST);
				 break;
			 case EST:
				 aventurier.setOrientation(Orientation.SUD);
				 break;
			 case OUEST:
				 aventurier.setOrientation(Orientation.NORD);
				 break;
			 }
			 break;
			 	
		 }
	}
	
	private static void moving(Aventurier aventurier, Node newNode, Logger logger) {
		if (newNode == null || !newNode.isFree()) {
			logger.info("Aventurier ne peut pas avancer");
		} else {
			Node actualNode = aventurier.getNode();
			actualNode.setFree(true);
			aventurier.setNode(newNode);
			newNode.setFree(false);
		
	}
}
	

}
