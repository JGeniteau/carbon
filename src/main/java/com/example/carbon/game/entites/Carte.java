package com.example.carbon.game.entites;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.carbon.game.TypeCase;

public class Carte {
	
	Logger logger = LoggerFactory.getLogger(Carte.class);
	private int nbAbscisse;
	private int nbOrdonnees;
	private List<Node> nodes;

	public Carte(int nbAbscisse, int nbOrdonnees) {
		nodes = new ArrayList<>();
		this.nbAbscisse = nbAbscisse;
		this.nbOrdonnees = nbOrdonnees;
		for (int i=0; i <nbAbscisse; i++) {
			for (int j=0; j<nbOrdonnees; j++) {
				nodes.add(new Node(i,j));
			}
        }
		logger.info("Carte créée");
	}
	
	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public int getNbAbscisse() {
		return nbAbscisse;
	}

	public void setNbAbscisse(int nbAbscisse) {
		this.nbAbscisse = nbAbscisse;
	}

	public int getNbOrdonnees() {
		return nbOrdonnees;
	}

	public void setNbOrdonnees(int nbOrdonnees) {
		this.nbOrdonnees = nbOrdonnees;
	}
	
	
	
}
