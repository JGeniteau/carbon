package com.example.carbon.game.entites;

import com.example.carbon.game.TypeCase;

public class Node {
	private int x;
	private int y;
	private TypeCase typeNode;
	private int nbTresors;
	private boolean isFree;
	
	public Node(int x, int y) {
		this.x = x;
		this.y = y;
		this.typeNode = TypeCase.PLAINE;
		this.isFree = true;
	}
	
	public Node(int x, int y, TypeCase typeCase, int nbTresors) {
		this.x = x;
		this.y = y;
		this.typeNode = typeCase;
		this.nbTresors = nbTresors;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public TypeCase getTypeCase() {
		return typeNode;
	}

	public void setTypeCase(TypeCase typeCase) {
		this.typeNode = typeCase;
	}

	public int getNbTresors() {
		return nbTresors;
	}

	public void setNbTresors(int nbTresors) {
		this.nbTresors = nbTresors;
	}
	
	public boolean isFree() {
		return isFree;
	}

	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}

	@Override
	public String toString() {
		return "Node [x=" + x + ", y=" + y + "]";
	}
	
	

}
