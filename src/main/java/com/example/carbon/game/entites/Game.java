package com.example.carbon.game.entites;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.carbon.game.Mouvement;
import com.example.carbon.game.Orientation;

public class Game {
	
	private Carte carte;
	private int tour;
	private List<Aventurier> aventuriers;
	
	public Game() {
		this.tour = 0;
		this.aventuriers = new ArrayList<>();
	}
	
	public Carte getCarte() {
		return carte;
	}
	public void setCarte(Carte carte) {
		this.carte = carte;
	}
	
	public List<Aventurier> getAventuriers() {
		return aventuriers;
	}
	public void setAventuriers(List<Aventurier> aventuriers) {
		this.aventuriers = aventuriers;
	}
	
	public int getTour() {
		return tour;
	}
	public void finDeTour() {
		this.tour+=1;
	}
	
	
}
