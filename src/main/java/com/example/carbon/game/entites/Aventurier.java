package com.example.carbon.game.entites;

import java.util.List;

import com.example.carbon.game.Mouvement;
import com.example.carbon.game.Orientation;

public class Aventurier {
	private String name;
	private Node node;
	private Node oldNode;
	private Orientation orientation;
	private List<Mouvement> mouvements;
	private int nbTresors;

	public Aventurier(String name, Node node, Orientation orientation, List<Mouvement> mouvements) {
		this.name = name;
		this.node = node;
		this.oldNode = node;
		this.orientation = orientation;
		this.mouvements = mouvements;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Orientation getOrientation() {
		return orientation;
	}
	
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
	
	public List<Mouvement> getMouvements() {
		return mouvements;
	}
	
	public void setMouvements(List<Mouvement> mouvements) {
		this.mouvements = mouvements;
	}
	
	public void addTresor() {
		this.nbTresors+=1;
	}

	public int getNbTresors() {
		return nbTresors;
	}

	public Node getOldNode() {
		return oldNode;
	}

	public void setOldNode(Node oldNode) {
		this.oldNode = oldNode;
	}

	@Override
	public String toString() {
		return "Aventurier [name=" + name + ", node=" + node + ", orientation=" + orientation + ", mouvements="
				+ mouvements + ", nbTresors=" + nbTresors + "]";
	}
	
	
	
	
	
}
