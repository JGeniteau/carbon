package com.example.carbon.game.services;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.carbon.game.GameHelper;
import com.example.carbon.game.entites.Aventurier;
import com.example.carbon.game.entites.Carte;
import com.example.carbon.game.entites.Game;
import com.example.carbon.game.entites.Node;

public class GameServicesImpl implements GameServices{
	
	Logger logger = LoggerFactory.getLogger(GameServicesImpl.class);
	
	private  static final String CARTE = "C";
	private  static final String MONTAGNE = "M";
	private  static final String TRESOR = "T";
	private  static final String AVENTURIER = "A";
	private  static final String COMMENT = "#";
	
	@Override
	public Game createGameContext(List<String> fileData) throws Exception{
		Game game = new Game();
		for(String line: fileData) {
			if (COMMENT.equals(line.substring(0, 1))){
				continue;
			}
			String[] listStr = line.split("-");
			List<String> newList = new ArrayList<>();
			for (String str: listStr) {
				str = str.replaceAll(" ","");
				newList.add(str);
			}
			switch(newList.get(0)) {
				case CARTE:
					Carte carte = new Carte(Integer.parseInt(newList.get(1)),Integer.parseInt(newList.get(2)));
					GameHelper.defineCarte(game, carte);
					break;
				
				case MONTAGNE:
					GameHelper.addMontagne(game.getCarte(), Integer.parseInt(newList.get(1)), Integer.parseInt(newList.get(2)), logger);
					break;
				
				case TRESOR:
					GameHelper.addTresor(game.getCarte(), Integer.parseInt(newList.get(1)), Integer.parseInt(newList.get(2)),Integer.parseInt(newList.get(3)), logger);
					break;
					
				case AVENTURIER:
					GameHelper.addAventurier(newList.get(1), Integer.parseInt(newList.get(2)), Integer.parseInt(newList.get(3)), newList.get(4), newList.get(5), game, logger);
					break;
					
				default:
					throw new Exception("Type d'objet inconnu.");
					
			}
		}
		return game;
		
	}
	
	@Override
	public void runGameTour(Game game) {
		for (Aventurier aventurier:game.getAventuriers()) {
			if (game.getTour() < aventurier.getMouvements().size()) {
				GameHelper.deplacementAventurier(aventurier, game.getTour(), game.getCarte(), logger);
				GameHelper.checkTresor(game, aventurier);
			}
		}
		game.finDeTour();
	}
	
	
}
