package com.example.carbon.game.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.example.carbon.game.TypeCase;
import com.example.carbon.game.entites.Aventurier;
import com.example.carbon.game.entites.Carte;
import com.example.carbon.game.entites.Game;
import com.example.carbon.game.entites.Node;

public class FileServicesImpl implements FileServices{
	
	@Override
	public List<String> readFile(String path) {
		Path filePath = Paths.get(path);
        Charset charset = StandardCharsets.UTF_8;

        try {
        	List<String> lines = Files.readAllLines(Paths.get(path));
            Files.lines(filePath, charset).forEach(System.out::println);
            return lines;

        } catch (IOException ex) {
            System.out.format("I/O error: %s%n", ex);
            return null;
        }
	}
	
	@Override
	public String getPathFileOut(Game game){
		String path = "out.txt";
		File fout = new File(path);
		try {
		FileOutputStream fos = new FileOutputStream(fout);
	 
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	 
		Carte carte = game.getCarte();
		
		bw.write(String.format("C - %d - %d",carte.getNbAbscisse(),carte.getNbOrdonnees()));
		bw.newLine();
		
		List<Node> nodeMontagnes = carte.getNodes().stream().filter(s-> TypeCase.MONTAGNE.equals(s.getTypeCase())).collect(Collectors.toList());
		for (Node node: nodeMontagnes) {
			bw.write(String.format("M - %d - %d",node.getX(),node.getY()));
			bw.newLine();
		}
		
		List<Node> nodeTresors = carte.getNodes().stream().filter(s-> TypeCase.TRESOR.equals(s.getTypeCase())).collect(Collectors.toList());
		for (Node node: nodeTresors) {
			bw.write(String.format("T - %d - %d - %d",node.getX(),node.getY(),node.getNbTresors()));
			bw.newLine();
		}
		
		List<Aventurier> aventuriers = game.getAventuriers();
		for (Aventurier aventurier: aventuriers) {
			Node node = aventurier.getNode();
			bw.write(String.format("A - %s - %d - %d - %s - %d",aventurier.getName(),node.getX(),node.getY(),aventurier.getOrientation().toString().substring(0, 1),aventurier.getNbTresors()));
			bw.newLine();
		}	
		
		bw.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path;
	}

}
