package com.example.carbon.game.services;

import java.util.List;

import com.example.carbon.game.entites.Game;

public interface GameServices {
	Game createGameContext(List<String> fileData) throws Exception;
	void runGameTour(Game game);
	
	
}
