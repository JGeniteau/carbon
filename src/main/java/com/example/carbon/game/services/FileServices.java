package com.example.carbon.game.services;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.example.carbon.game.entites.Game;

public interface FileServices {
	List<String> readFile(String path);
	String getPathFileOut(Game game);
}
