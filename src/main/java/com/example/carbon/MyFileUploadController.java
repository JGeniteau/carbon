package com.example.carbon;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.carbon.game.Main;
import com.example.carbon.game.services.FileServices;
import com.example.carbon.game.services.FileServicesImpl;
import com.example.carbon.game.services.GameServices;
import com.example.carbon.game.services.GameServicesImpl;


@Controller
public class MyFileUploadController {

   @RequestMapping(value = "/")
   public String homePage() {

      return "index";
   }

   // GET: Show upload form page.
   @RequestMapping(value = "/uploadOneFile", method = RequestMethod.GET)
   public String uploadOneFileHandler(Model model) {

      MyUploadForm myUploadForm = new MyUploadForm();
      model.addAttribute("myUploadForm", myUploadForm);

      return "uploadOneFile";
   }

   // POST: Do Upload
   @RequestMapping(value = "/uploadOneFile", method = RequestMethod.POST)
   public String uploadOneFileHandlerPOST(HttpServletRequest request, //
         Model model, //
         @ModelAttribute("myUploadForm") MyUploadForm myUploadForm) {

      return this.doUpload(request, model, myUploadForm);

   }
  

// GET: Show upload form page.
   @RequestMapping(value = "/runGame", method = RequestMethod.GET)
   public String runGame(Model model) {
	   
	  

      return "uploadMultiFile";
   }

   private String doUpload(HttpServletRequest request, Model model, //
         MyUploadForm myUploadForm) {

      String description = myUploadForm.getDescription();
      System.out.println("Description: " + description);

      // Root Directory.
      String uploadRootPath = request.getServletContext().getRealPath("upload");
      System.out.println("uploadRootPath=" + uploadRootPath);

      File uploadRootDir = new File(uploadRootPath);
      // Create directory if it not exists.
      if (!uploadRootDir.exists()) {
         uploadRootDir.mkdirs();
      }
      MultipartFile[] fileDatas = myUploadForm.getFileDatas();
      //
      List<File> uploadedFiles = new ArrayList<File>();
      List<String> failedFiles = new ArrayList<String>();
      List<String> linesData = new ArrayList<String>();
      List<String> linesOutput = new ArrayList<String>();
      for (MultipartFile fileData : fileDatas) {

         // Client File Name
         String name = fileData.getOriginalFilename();
         System.out.println("Client File Name = " + name);

         if (name != null && name.length() > 0) {
            try {
               // Create the file at server
               File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

               BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
               stream.write(fileData.getBytes());
               stream.close();
               //
               uploadedFiles.add(serverFile);
               System.out.println("Write file: " + serverFile);
               
              FileServices fileServices = new FileServicesImpl();
              GameServices gameServices = new GameServicesImpl();
         	  linesData = fileServices.readFile(serverFile.getAbsolutePath());
         	  try {
         		 linesOutput = Main.runGame2(linesData, gameServices, fileServices);
         	} catch (Exception e) {
         		model.addAttribute("ex", e);
         		e.printStackTrace();
         	}
         	  
         	  
            } catch (Exception e) {
               System.out.println("Error Write file: " + name);
               failedFiles.add(name);
            }
         }
      }
      model.addAttribute("description", description);
      model.addAttribute("uploadedFiles", uploadedFiles);
      model.addAttribute("failedFiles", failedFiles);
      model.addAttribute("dataIn", linesData);
      model.addAttribute("dataOut", linesOutput);
      
      
	  
      return "uploadResult";
   }

}